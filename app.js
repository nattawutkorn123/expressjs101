var express = require('express');
var app = express();
var cors = require('cors')
var bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());
app.use(bodyParser.raw());

async function httpsget(params) {
    console.log(params)
    return "testfunction";
}

app.use(cors({ credentials: true, origin: ['http://localhost:4200'] }))
app.post('/', async function(req, res) {
    const tmp_data = await httpsget(req.body)
    console.log(tmp_data)
    res.send({ data: req.body, title: "ioioioioioio", return: tmp_data });

});
app.listen(3000, function() {
    console.log('Example app listening on port 3000!');
});